import difflib

import pandas as pd

from indexing import retriever

df = pd.read_csv('data/fcdb.tsv', sep='\t')
food_labels = pd.read_csv('data/food-labels.tsv', sep='\t')
# join the two dataframes
df = df.merge(food_labels, left_on='FOOD_ID', right_on='id', how='left')


def get_food_id(food_name, results=3, cutoff=0.6):
    # Perform the matching using difflib's get_close_matches
    # Using the 'NAME_SLO' column for Slovenian names

    candidates = retriever.retrieve(food_name, top_k=50)
    candidate_names = [c['NAME_SLO'] for c in candidates if 'NAME_SLO' in c and c['NAME_SLO'] is not None]

    matches = difflib.get_close_matches(food_name, candidate_names, n=results, cutoff=cutoff)

    if matches:
        food_matches = []
        for match in matches:
            # find the food_id for the matched name
            food_id = [c['FOOD_ID'] for c in candidates if c['NAME_SLO'] == match][0]
            food_matches.append({'food_id': food_id, 'matched_name': match})
        return food_matches
    else:
        return {'error': 'No matching food found'}


def healthier_options(food_id, res_num=5):
    """
    Get healthier options for a given food ID.

    Parameters:
    food_id (str): The food ID to find healthier options for.
    results (int): The number of healthier options to return.

    Returns:
    list: A list of dictionaries containing the food ID and name of the healthier options.
    """
    # Placeholder implementation

    fcdb_item = df[df['FOOD_ID'] == food_id]
    # check if there are results
    if fcdb_item.empty:
        return {'error': 'No food item found with the given food_id'}

    food_name = fcdb_item['NAME_SLO'].values[0]
    food_group = fcdb_item['FOOD_GROUP'].values[0]
    # check if food name is None or nan
    if pd.isnull(food_name):
        food_name = fcdb_item['SHORT_NAME_SLO'].values[0]

    if pd.isnull(food_name):
        food_name = fcdb_item['NAME_ENG'].values[0]

    candidates = retriever.retrieve(f'{food_name} \t {food_group}', top_k=20)

    # select candidates containing nutriscore
    candidates = [c for c in candidates if
                  'tl_score' in c and c['tl_score'] is not None and c['FOOD_GROUP'] == food_group]
    # sort first by score, then by nutriscore
    candidates.sort(key=lambda x: x['tl_score'])

    # transform the results in format: {'food_id': food_id, 'name': name, 'nutriscore': nutriscore, 'sim_score': score}
    results = []
    for c in candidates:
        name = c['NAME_SLO']
        if name is None:
            name = c['SHORT_NAME_SLO']
        if name is None:
            name = c['NAME_ENG']
        results.append({
            'food_id': c['FOOD_ID'],
            'name': name,
            'group': c['FOOD_GROUP'],
            'nutriscore': c['nutriscore'],
            'tl_score': c['tl_score'],
            'sim_score': c['score']
        })

    return results[:res_num]
