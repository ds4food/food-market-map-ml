try:
    from flask import Flask, request, jsonify
    import os
    import datetime
    from c2_barcode import read_barcode
    from find_fcdb_match import get_food_id, healthier_options
    from indexing import retriever
except ImportError as e:
    print(f"ImportError: {e}")
    raise


app = Flask(__name__)


@app.route('/read_barcode', methods=['POST'])
def handle_barcode_reading():
    # Check if a file is included in the POST request
    if 'image' not in request.files:
        return jsonify({"error": "No image file provided"}), 400

    image_file = request.files['image']

    # Save the file temporarily
    temp_path = os.path.join(f"temp_image_{datetime.datetime.now().strftime('%Y%m%d_%H%M%S')}.png")
    image_file.save(temp_path)

    # Read the barcode from the saved image
    barcode_data = read_barcode(temp_path)

    # Remove the temporary file
    os.remove(temp_path)

    if barcode_data:
        return jsonify({"barcode_data": barcode_data})
    else:
        return jsonify({"error": "No barcode found"}), 404


@app.route('/get_fcdb_id', methods=['GET'])
def get_fcdb_id():
    food_name = request.args.get('food_name', '').strip()

    if not food_name:
        return jsonify({'error': 'No food name provided'}), 400

    # get query parameter cutoff as float
    cutoff = request.args.get('cutoff', 0.6)

    results = request.args.get('results', 3)
    try:
        cutoff = float(cutoff)
        results = int(results)
    except ValueError:
        return jsonify({'error': 'Invalid cutoff value. Must be a float.'}), 400

    return jsonify(get_food_id(food_name, results, cutoff))


@app.route('/healthier_options', methods=['GET'])
def get_healthier_options():
    food_id = request.args.get('food_id')

    if not food_id:
        return jsonify({'error': 'No food ID provided'}), 400

    results = request.args.get('results', 3)
    try:
        results = int(results)
    except ValueError:
        return jsonify({'error': 'Invalid results value. Must be an integer.'}), 400

    return jsonify(healthier_options(food_id, results))

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)