import base64
import hashlib
import os
import sys
from typing import List, Union

import cv2
import numpy as np
import pytesseract
from openai import OpenAI
from pydantic import BaseModel, field_validator

api_key = os.getenv('OPENAI_API_KEY')
print(api_key)
client = OpenAI(api_key=api_key)


def get_filtered_average_font_size(image, threshold_factor=2.0):
    # Perform OCR to get bounding boxes
    h, w = image.shape
    boxes = pytesseract.image_to_boxes(image)

    # Initialize a list to hold character heights
    heights = []

    # Parse the bounding box data
    for box in boxes.splitlines():
        box = box.split()
        char, x, y, x2, y2 = box[0], int(box[1]), int(box[2]), int(box[3]), int(box[4])
        height = y2 - y
        # print(height)
        heights.append(height)

    # Calculate the initial average height and standard deviation
    if not heights:
        return 0
    average_height = np.mean(heights)
    std_dev_height = np.std(heights)

    # Define the threshold
    threshold = average_height + threshold_factor * std_dev_height

    # Filter out boxes that are significantly larger than the average
    filtered_heights = [height for height in heights if height <= threshold]

    # Recalculate the average height using the filtered heights
    filtered_average_height = np.mean(filtered_heights) if filtered_heights else 0

    return filtered_average_height


def preprocess_image(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, encoded_png = cv2.imencode('.png', gray)
    gray = cv2.imdecode(encoded_png, cv2.IMREAD_GRAYSCALE)
    average_font_size = get_filtered_average_font_size(gray, 1.5)
    scale_factor = 18 / average_font_size;
    if scale_factor > 1.1:
        gray = rescale_image(gray, scale_factor)
    #
    # blurred = cv2.medianBlur(gray, 3)
    # #         display_image('Bilateral Filter', image)
    # kernel = np.ones((2, 2), np.uint8)
    # eroded = cv2.erode(blurrkernel, iterations=2)
    # dilated = cv2.dilate(eroded, kernel, iterations=2)ed,
    binary = cv2.adaptiveThreshold(gray.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 25,
                                   17)
    return binary


def rescale_image(image, scale_factor):
    # Calculate the new dimensions
    width = int(image.shape[1] * scale_factor)
    height = int(image.shape[0] * scale_factor)
    new_size = (width, height)

    # Resize the image
    resized_image = cv2.resize(image, new_size, interpolation=cv2.INTER_LINEAR)
    return resized_image


def create_image_hash(image_bytes):
    # Create a hash object
    hash_object = hashlib.sha256(image_bytes)
    # Get the hexadecimal digest of the hash
    hex_digest = hash_object.hexdigest()
    return hex_digest


import re
import json


def correct_product(product):
    if product:
        product = re.sub(r'([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z]+\.)', r'\1 ', product)
        product = re.sub(
            r'([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z]+)( )(\'[\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z]+)',
            r'\1\3 ',
            product)
    return product


def correct_package(package):
    if package:
        package = re.sub(r'(k9|KG)$', 'kg', package)
        package = re.sub(r'G$', 'g', package)
        g_match = re.search(r'^\d+[05][693a;]', package)
        if (g_match):
            package = re.sub(r'[369a;]$', 'g', package)
        package = re.sub(r'[zZ]\s?[xX]', '2x', package)
        package = re.sub(r'(m1|HL|m\])$', 'ml', package)
        package = re.sub(r'2m.m$', '%m.m', package)
        # package = re.sub(r'\d+[05][369a]S$','9', package)
        package = re.sub(r',', '.', package)
    return package


def get_package_quantity_price_orig(line):
    # package_reg_ex = r'^(\d+|\d+[\.,]\d+)[0124578]$'
    package_reg_ex = r'^(\d+[05][693a;]|(\d+|\d+[\.,]\d+)\s?[a-zA-Z%]+)'
    quantity_reg_ex = r'\d+'
    # price_reg_ex = r'(\d[\.,]\d{2,3}\s?[xbABC08€£]?(\sx\.)?|\d{4,5}\s?[xbABC08€£\.]?(\s[x4]?[\.:]?))$'
    price_reg_ex = r'[0-9oOBDzZil][\.,][0-9oOBDzZil]{2,3}$'
    quantity = ''
    package = ''
    price = ''
    price_match = re.search(price_reg_ex, line)
    if price_match:
        price = get_price(price_match.group(0))
        # print('price '+price)
        line = line[0:price_match.start()]

    quantity_start_index = 0
    package_match = re.search(package_reg_ex, line)
    if package_match:
        package = correct_package(package_match.group(0))
        print('PACKAGE ' + package_match.group(0) + '-->' + correct_package(package_match.group(0)))
        quantity_start_index = package_match.end();
    quantity_match = re.search(quantity_reg_ex, line[quantity_start_index:])
    if quantity_match:
        quantity = quantity_match.group(0)
    return (correct_package(package), correct_number(quantity), correct_number(price))


def get_item_package_quantity_price(start_of_line, rest_of_line):
    # package_reg_ex = r'^(\d+|\d+[\.,]\d+)[0124578]$'
    package_reg_ex = r'(\d+[0Oo5][693a;]|\d+\/\d+|(\d+|\d+[\.,]\d+)\s?[kgmliKGMLHc]+)'
    quantity_reg_ex = r'\d+'
    # price_reg_ex = r'(\d[\.,]\d{2,3}\s?[xbABC08€£]?(\sx\.)?|\d{4,5}\s?[xbABC08€£\.]?(\s[x4]?[\.:]?))$'
    price_reg_ex = r'[0-9oOBDzZil][\.,][0-9oOBDzZil]{2,3}$'
    product = start_of_line
    quantity = '1'
    package = ''
    price = ''
    price_match = re.search(price_reg_ex, rest_of_line)
    if price_match:
        price = get_price(price_match.group(0))
        # print('price '+price)
        rest_of_line = rest_of_line[0:price_match.start()]

    quantity_start_index = 0
    package_match = re.search(package_reg_ex, start_of_line)
    if package_match:
        package = correct_package(package_match.group(0))
        print('PACKAGE ' + package_match.group(0) + '-->' + correct_package(package_match.group(0)))
        product = re.sub(package_reg_ex, '', start_of_line)
    quantity_match = re.search(quantity_reg_ex, rest_of_line)
    if quantity_match:
        quantity = quantity_match.group(0)
    return (correct_product(product), correct_package(package), correct_number(quantity), correct_number(price))


def get_market(line):
    markets = [('Tuš', r'Tuš|Tus|tusslovenija'),
               ('DM', r'dm|drogerie|DM'),
               ('Spar', r'[ŠSsš]par'),
               ('Eurospin', r'[Ee]urospin'),
               ('Kompas', r'Kompas|Shop'),
               ('Hofer', r'HOFER'),
               ('Lidl', r'Lidl'),
               ('Mercator', r'[Mm][se]r[ck]ator')
               ]
    for (market, reg_ex) in markets:
        # print(market + ' ' +reg_ex)
        match = re.search(reg_ex, line)
        if match:
            # print(reg_ex +">>>"+match.group(0))
            return market
    return None


def get_price(line):
    price_errors = [
        ('x.xx__', r'[0-9oDOzZil][\.,][0-9oODzZil]{2}(\s|\.)?[xbABC0D8€£4\.]?(\s[x4z]?[\.:]?)?$'),
        ('xxx__', r'[0-9oODzZil]{4,5}(\s|\.)?[xbABC0D8€£4]?:?$'),
        # ('k9', r'(\d{3}|\d[\.,]\d{2})\s?[xbABC08€]?\.?$'),
    ]
    # print(line)
    for (error, reg_ex) in price_errors:
        # print(error + ' ' +reg_ex)
        match = re.search(reg_ex, line)
        if match:
            # print(reg_ex+'>>>'+match.group(0))
            if error == 'x.xx__':
                return line[match.start():match.start() + 4]
            elif error == 'xxx__':
                return line[match.start()] + '.' + line[match.start() + 1:match.start() + 3]
    return ''


def is_not_blank(line):
    return len(line.strip()) > 0


def correct_number(price):
    if price:
        price = re.sub(r'[Zz]', '2', price)
        price = re.sub(r'[DOo]', '0', price)
        price = re.sub(r'S$', '9', price)
        price = re.sub(r',', '.', price)
    return price


# def correct_product(item):
#   if item:
#     item = re.sub(r'^[ABCDE]\s((4|H|ii|ti|dt|£)\s)?','', item)
#   return item
def split_item(line):
    # item_reg_ex = r'([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]+[\.\',:]?\s?)+'

    # item_reg_ex = r'^([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z£\.\',]{1,}[\.\',:]?\s?)+([0-9]+\s+)?([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)*'
    # item_reg_ex = r'^([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z£\.\',]{1,}[\.\',:]?\s?)+([0-9]+\s+([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)+|([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)*)'
    # item_reg_ex =r'^[\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z]{1,}(\s+(|[0-9oODzZil]+[a-zA-Z]+|[0-9oODzZil]+|[\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]+))*(\s+([0-9oODzZil]+[a-zA-Z]{1,2}|[\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]+))*'
    item_reg_ex = r'^[\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z]+[\w\W]*([0-9oODzZil]+[a-zA-Z]+|\d+[0Oo5][693a;]|\d+\/\d+)'
    # qunatity_reg_ex = r'^(\d+|\d+[\.,]\d+)[^369]$'
    # price_reg_ex = r'(\d{3}|\d[\.,]\d{2})\s?[xbABC08€]?\.?$'\
    # second_line_reg_ex=r'^([0-9zZDo]+[\.,][0-9zZDo]{2,}|[0-9zZDo]+\s?\[^0-9]|[1-9zZ]+\s?[xX]|\-\d+)'
    second_line_reg_ex = r'^([0-9zZDo]+[\.,][0-9zZDo]{2,}|[1-9zZ]+\s?[xX]|\-\d+)'
    second_line_quantity_reg_ex = r'^([0-9zZDo]+[\.,][0-9zZDo]+|[0-9oOzZD]+)(\s?[a-z]{2,3})?'
    product = ''
    package = ''
    quantity = ''
    price = ''
    first_line = True;
    # main_line = True
    # match item line
    # print('inside '+line)
    second_line_match = re.search(second_line_reg_ex, line)
    if second_line_match:
        print('SECOND LINE ' + line)
        first_line = False;
        quantity_match = re.search(second_line_quantity_reg_ex, line)
        if quantity_match:
            quantity = correct_number(quantity_match.group(0))
            price = correct_number(get_price(line[quantity_match.end():]))
    else:
        match = re.search(item_reg_ex, line, re.UNICODE)
        # print(match)
        if (match):
            print('ITEM: ' + match.group(0))

            product = match.group(0)
            start_of_item = line[0:match.end()]
            rest_of_item = line[match.end():]
            # product may be modified by removing the package info
            (product, package, quantity, price) = get_item_package_quantity_price(start_of_item, rest_of_item)

    return (correct_product(product), package, quantity, price, first_line)


def ocr_receipt_file_to_json(file_path):
    # Define a list of known units of measurement
    with open(file_path, 'r', encoding='utf-8') as file:
        file_content = file.read()
    return ocr_receipt_to_json(file_content)


# def process_market()
def ocr_receipt_to_json(receipt_txt):
    # Define a list of known units of measurement
    item_begin_reg_ex = '([ČCcč]ena|Artikel|[Kk]oličina|Kol\.|Vrednost|[Zz]nesek|[Ii]zdelek|ELJR|^\s*EUR\s*$)'
    # item_skip_reg_ex = '(^[^0-9]+\s*:\s+|[Pp]opust|[Kk]upon|[Pp]rihranek|[Aa]kcija|klub)'
    item_skip_reg_ex = '([Pp]opust|[Kk]upon|[Pp]rihranek|[Aa]kcija|klub)'
    footer_begin_regex = '(Attendant|AUtenidant|enidant|ECR|V[IT]SA|[Ss]?k?upa[jJ]|[Zz]?a plačilo|[Zz]nesek|[Kk]artica|[Pp]otrdilo|[Kk]upca|Super cena|Način|Pridobili)'
    position = {
        "market": True,
        "header": False,
        "items": False,
        "footer": False
    }
    market_found = False
    json_receipt = {"market": "",
                    "items": []}
    # print(pattern)
    json_items = []
    item_index = -1;
    lines = receipt_txt.splitlines()
    line_index = 0
    # check header for market and start of items
    for line in lines:
        if is_not_blank(line):
            market = get_market(line)
            if market:
                json_receipt["market"] = market;
                break
    for line in lines:
        line_index += 1
        match = re.search(item_begin_reg_ex, line)
        if match:
            print('ITEMS:' + line)
            position["items"] = True
            position["header"] = False
            break
    if line_index == len(lines):
        line_index = 0
        position["items"] = True
    # iterate through items (even if there is not math for market or items header)
    for line in lines[line_index:]:
        if is_not_blank(line):
            if position["items"]:
                # print(line)
                if re.search(item_skip_reg_ex + '|' + item_begin_reg_ex, line):
                    print('IGNORED:' + line)
                    continue
                # print(line)
                matches_items = re.findall(footer_begin_regex, line)
                if len(matches_items) > 0:
                    # print(matches_items)
                    position["items"] = False
                    position["footer"] = True
                    break
                (product_name, package_info, quantity, price, main_line) = split_item(line)
                if main_line:
                    json_node = {
                        "name": product_name,
                        "package": package_info,
                        "quantity": quantity,
                        "price": price
                    }
                    if (product_name == ''):
                        continue
                    json_items.append(json_node)
                    item_index += 1
                else:
                    if (item_index >= 0):
                        json_items[item_index]["quantity"] = quantity
                        if json_items[item_index]["price"] == '':
                            json_items[item_index]["price"] = price
    json_receipt["items"] = json_items
    return json_receipt


def refine_text(ocr_text):
    # Replace common OCR errors
    ocr_text = re.sub(r'\n+', '\n', ocr_text, flags=re.MULTILINE)  # Replace multiple newlines with a single newline
    ocr_text = re.sub(r' +', ' ', ocr_text, flags=re.MULTILINE)  # Replace multiple spaces with a single space
    # ocr_text = re.sub(r' [\|]', '', ocr_text, flags=re.MULTILINE)   # Remove |
    ocr_text = re.sub(r' [\|\.\':<!\-\(\)%«;€£ ]+$', '', ocr_text, flags=re.MULTILINE)  # Remove single . or ' on end
    ocr_text = re.sub(r'(\s+(\d|[a-zA-Z])[:.»«a-z]\s*)+$', '', ocr_text, flags=re.MULTILINE)  # Remove 4: or 4.
    ocr_text = re.sub(r'(\s+[:.»«](\d|[a-zA-Z])\s*)+$', '', ocr_text, flags=re.MULTILINE)  # Remove 4: or 4.

    ocr_text = re.sub(r'^[\'\. ]+', '', ocr_text, flags=re.MULTILINE)  # Remove ' or . at the beginning

    ocr_text = re.sub(r'(\w+)[\.\':]$', r'\1', ocr_text, flags=re.MULTILINE)  # Remove single . or ' on end
    ocr_text = re.sub(r'\s+(\d|\w|€|£)$', '', ocr_text, flags=re.MULTILINE)  # Remove digit or letter on end
    ocr_text = re.sub(r'([\.\:\!])([\.\:\!])', r'\1', ocr_text, flags=re.MULTILINE)  # Remove double special characters
    ocr_text = re.sub(r'(\d)[ABC]$', r'\1', ocr_text, flags=re.MULTILINE)
    # ocr_text = re.sub(r'[€£]$', '', ocr_text, flags=re.MULTILINE)   # Remove €£ on end
    ocr_text = re.sub(r'(\w+)[\.\':]$', r'\1', ocr_text, flags=re.MULTILINE)  # Remove single . or ' on end
    ocr_text = re.sub(r'(\d[\.,]\d\d)\d$', r'\1', ocr_text, flags=re.MULTILINE)  # Remove last digit from x.xxx$
    ocr_text = re.sub(r'^[ABCD]\s+', '', ocr_text, flags=re.MULTILINE)  # Remove single capital letter at beginning
    ocr_text = re.sub(r'^\s+', '', ocr_text, flags=re.MULTILINE)  # Remove leading spaces
    ocr_text = re.sub(r'\s+$', '', ocr_text, flags=re.MULTILINE)  # Remove ending spaces
    # ocr_text = re.sub(r'(\d)\s+(\d)', r'\1\2', ocr_text)  # Remove spaces between digits
    # ocr_text = re.sub(r'\s?,\s?', ',', ocr_text)  # Remove spaces around commas
    # ocr_text = re.sub(r'\s?\.\s?', '.', ocr_text)  # Remove spaces around periods
    return ocr_text


class Item(BaseModel):
    name: str
    package: str
    quantity: Union[float, str, None] = None
    price: Union[float, str, None] = None

    # Convert quantity to float if it's a string
    @field_validator('quantity', 'price', mode='before')
    def convert_to_float(cls, v):
        if isinstance(v, str):
            try:
                return float(v)
            except ValueError:
                raise ValueError(f"Cannot convert '{v}' to float")
        if isinstance(v, int):
            return float(v)
        return v


class Receipt(BaseModel):
    market: str
    items: List[Item]


def ocr_binary_to_json(binary):
    custom_config = r'--oem 3 --psm 4'
    # custom_config = r'--oem 3 --psm 6'
    extracted_text = pytesseract.image_to_string(binary, lang='slv', config=custom_config)
    extracted_text = refine_text(extracted_text)
    return ocr_receipt_to_json(extracted_text)


def ocr_binary_to_json_tesseract_gpt(binary):
    custom_config = r'--oem 3 --psm 4'
    # custom_config = r'--oem 3 --psm 6'
    extracted_text = pytesseract.image_to_string(binary, lang='slv', config=custom_config)
    model = 'gpt-4o'
    prompt = """The input is a receipt data in slovenian language obtained from tesseract ocr. Correct what ocr did not manage to recognize well. Return only the JSON object  and nothing else.
    If a row in the input text has no product name it contains data in the form quantity x price per unit. Ignore items with name kupon or prihranek.
     The receipt should have the following structure: 
     {market:"string",
      items:[{name:"string",package:"string", quantity:'float',price:"float"}]
     }
      market is the  name of the receipt issuer, 
      name is the item name,
      package (if any) is a numeric value and unit (g, kg, ml, L, l and so on).
      default value for quantity is 1.0
      price is total price"""

    gpt_response = ask_gpt_tesseract_text(extracted_text, model, prompt)
    print('Total tokens', gpt_response.usage.total_tokens, file=sys.stderr)

    dirty_json = gpt_response.choices[0].message.content
    cleaned_json = dirty_json.replace("```json", "").replace("```", "").strip()

    cleaned_json = json.loads(cleaned_json)

    # Validate the JSON structure with Pydantic
    cleaned_json = Receipt(**cleaned_json)
    # Convert the cleaned text response to a JSON object
    return cleaned_json.dict()


def ocr_binary_to_json_gpt(image):
    model = 'gpt-4o-2024-08-06'
    prompt = """Return only a json object that represents receipt data in slovenian language obtained from tesseract ocr. Return only the JSON object and nothing else.
  If a row in the input text has no product name it contains data in the form quantity x price per unit. Ignore items with name kupon or prihranek.
   The receipt should have the following structure: 
   {market:"string",
    items:[{name:"string",package:"string", quantity:"float",price:"float"}]
   }
    market is the  name of the receipt issuer, 
    name is the item name,
    package (if any) is a numeric value and unit (g, kg, ml, L, l and so on).
    default value for quantity is 1.0
    price is total price"""

    gpt_response = ask_gpt_preprocimage64(image, model, prompt, Receipt)
    print(gpt_response)

    return gpt_response.dict()
    # return gpt_response


def ask_gpt_preprocimage64(image, model, prompt, response_format):
    # Getting the base64 string
    retval, buffer = cv2.imencode('.jpg', image)
    image_bytes = buffer.tobytes()
    base64_image = base64.b64encode(image_bytes).decode('utf-8')

    response = client.beta.chat.completions.parse(
        model=model,
        messages=[
            {
                "role": "user",
                "content": [
                    {
                        "type": "text",
                        "text": prompt
                    },
                    {
                        "type": "image_url",
                        "image_url": {
                            "url": f"data:image/jpeg;base64,{base64_image}"
                        }
                    }
                ]
            }
        ],
        response_format=response_format
    )
    print('obtained openai response')
    return response.choices[0].message.parsed


def ask_gpt_tesseract_text(text, model, prompt):
    completion = client.chat.completions.create(
        model=model,
        messages=[
            {"role": "system", "content": prompt},
            {"role": "user", "content": text}
        ]
    )
    return completion
