import sys
import time

import cv2
import numpy as np
from flask import Flask, request, jsonify
from flask import send_from_directory

import ocr

app = Flask(__name__)


@app.route('/receipts/<path:path>')
def send_report(path):
    print(path)
    return send_from_directory('images', f'{path}')


@app.route('/read-receipt-tesseract', methods=['POST'])
def upload_image():
    # Read the file contents
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400

    file = request.files['file']

    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400

    try:
        timestamp_seconds = int(time.time())
        name = f'{timestamp_seconds}-{file.filename}'
        file_path = f'images/{name}'
        print(file_path)
        file.save(file_path)

        # Reload the file from disk for processing
        with open(file_path, 'rb') as f:
            contents = f.read()

        nparr = np.frombuffer(contents, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        if img is None:
            return jsonify({"error": "Cannot decode image"}), 400

        binary = ocr.preprocess_image(img)
        json_receipt = ocr.ocr_binary_to_json(binary)
        json_receipt["path"] = f'receipts/{name}'

        return jsonify(json_receipt)
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route('/read-receipt', methods=['POST'])
def upload_image_gpt():
    # Read the file contents
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400
    file = request.files['file']
    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400
    try:
        timestamp_seconds = int(time.time())
        name = f'{timestamp_seconds}-{file.filename}'
        file_path = f'images/{name}'
        print(file_path)
        file.save(file_path)

        # Reload the file from disk for processing
        with open(file_path, 'rb') as f:
            contents = f.read()
        # image_hash=str(ocr.create_image_hash(contents))
        nparr = np.frombuffer(contents, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        if img is None:
            return jsonify({"error": "Cannot decode image"}), 400
        t1 = time.time()
        binary = ocr.preprocess_image(img)
        t2 = time.time()
        preprocess_time = t2 - t1

        json_receipt = ocr.ocr_binary_to_json_gpt(binary)
        t1 = time.time()
        gpt_time = t1 - t2
        print(f'Time: {preprocess_time + gpt_time} = {preprocess_time} + {gpt_time} seconds', file=sys.stderr)

        json_receipt["path"] = f'receipts/{name}'
        return jsonify(json_receipt)
    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route('/read-receipt-tesseract-gpt', methods=['POST'])
def upload_image_tesseract_gpt():
    # Read the file contents
    if 'file' not in request.files:
        return jsonify({"error": "No file part"}), 400
    file = request.files['file']
    if file.filename == '':
        return jsonify({"error": "No selected file"}), 400
    try:
        contents = file.read()
        # image_hash=str(ocr.create_image_hash(contents))
        nparr = np.frombuffer(contents, np.uint8)
        img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

        if img is None:
            return jsonify({"error": "Cannot decode image"}), 400
        t1 = time.time()
        binary = ocr.preprocess_image(img)
        t2 = time.time()
        preprocess_time = t2 - t1

        json_receipt = ocr.ocr_binary_to_json_tesseract_gpt(binary)
        t1 = time.time()
        gpt_time = t1 - t2
        print(f'Time: {preprocess_time + gpt_time} = {preprocess_time} + {gpt_time} seconds', file=sys.stderr)

        return jsonify(json_receipt)
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
