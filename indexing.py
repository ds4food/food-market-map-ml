import json

# from llama_index.llms.ollama import Ollama
import faiss
import pandas as pd
from llama_index.core import VectorStoreIndex, Settings, load_index_from_storage, StorageContext
from llama_index.embeddings.huggingface import HuggingFaceEmbedding
from llama_index.readers.file import PandasCSVReader
from llama_index.vector_stores.faiss import FaissVectorStore

# bge-base embedding model
Settings.embed_model = HuggingFaceEmbedding(model_name='BAAI/bge-base-en-v1.5')
print('embed model loaded')


class FcdbRetriever:
    def __init__(self):
        vector_store = FaissVectorStore.from_persist_dir("./storage")
        storage_context = StorageContext.from_defaults(vector_store=vector_store, persist_dir="./storage")
        self.ix = load_index_from_storage(storage_context=storage_context)
        self.food_labels = pd.read_csv("data/food-labels_tl.tsv", sep='\t')
        self.fcdb = pd.read_csv("data/fcdb.tsv", sep='\t')
        print('index loaded from storage')

    def build_and_store_index(self):
        # Faiss config
        d = 768
        faiss_index = faiss.IndexFlatL2(d)

        # Vector store config
        vector_store = FaissVectorStore(faiss_index=faiss_index)
        storage_context = StorageContext.from_defaults(vector_store=vector_store)

        # Load data
        csv_reader = PandasCSVReader(concat_rows=False, pandas_config={"sep": '\t'})
        rows = csv_reader.load_data("data/fcdb.tsv")

        # Build index
        index = VectorStoreIndex.from_documents(rows, storage_context=storage_context, show_progress=True)
        print('index loaded')

        # Persist index
        index.storage_context.persist("./storage")
        print('index persisted')

    def retrieve(self, text, top_k=5):
        query_retriever = self.ix.as_retriever(similarity_top_k=top_k)
        results = query_retriever.retrieve(text)
        out = []
        for r in results:
            fcdb_id = r.text.split(",")[0]
            foods = json.loads(self.fcdb[self.fcdb['FOOD_ID'] == fcdb_id].to_json(orient='records'))

            if len(foods) > 0:
                result = foods[0]
                labels = json.loads(self.food_labels[self.food_labels['id'] == fcdb_id].to_json(orient='records'))
                if len(labels) > 0:
                    result['v_label'] = labels[0]['v_label']
                    result['nutriscore'] = labels[0]['nutriscore']
                    result['tl_score'] = labels[0]['tl_score']

                result['score'] = r.score
                out.append(result)

        return out


retriever = FcdbRetriever()

# Requirements!!!
# llama-index
# llama-index-embeddings-huggingface
# faiss-cpu
# llama-index-vector-stores-faiss
# llama-index-llms-ollama
# accelerate
