import re
import json
import hashlib
import cv2
import numpy as np
from PIL import Image
import io


def binarize_receipt(img):
      gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
      # Thresholding image with based on local values
      binary = cv2.adaptiveThreshold(gray.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 21,
                                     13)
      # cv2.imwrite(dest_dir + purename + extension, binary)
      return binary

def create_image_hash(image_bytes):
  # Create a hash object
  hash_object = hashlib.sha256(image_bytes)
  # Get the hexadecimal digest of the hash
  hex_digest = hash_object.hexdigest()
  return hex_digest

def correct_package(package):
  if package:
    package = re.sub(r'(k9|KG)$','kg', package)
    package = re.sub(r'G$','g', package)
    g_match = re.search(r'^\d+[05][693a]',package)
    if (g_match):
        package = re.sub(r'[369a]$','g', package)
    package = re.sub(r'[zZ]\s?[xX]','2x', package)
    package = re.sub(r'(m1|HL|m\])$','ml', package)
    package = re.sub(r'2m.m$','%m.m', package)
    # package = re.sub(r'\d+[05][369a]S$','9', package)
    package = re.sub(r',','.', package)
  return package

def get_package_quantity_price(line):
  # package_reg_ex = r'^(\d+|\d+[\.,]\d+)[0124578]$'
  package_reg_ex = r'^(\d+[05][693a]|(\d+|\d+[\.,]\d+)\s?[a-zA-Z%]+)'
  quantity_reg_ex = r'\d+'
  price_reg_ex = r'(\d[\.,]\d{2,3}\s?[xbABC08€£]?(\sx\.)?|\d{4,5}\s?[xbABC08€£\.]?(\s[x4]?[\.:]?))$'
  quantity = ''
  package = ''
  price=''
  price_match = re.search(price_reg_ex, line)
  if price_match:
    price = get_price(price_match.group(0))
    # print('price '+price)
    line =  line[0:price_match.start()]
  quantity_start_index=0
  package_match = re.search(package_reg_ex,line)
  if package_match:
    package=correct_package(package_match.group(0))
    print('PACKAGE '+package_match.group(0)+'-->'+correct_package(package_match.group(0)))
    quantity_start_index=package_match.end();
  quantity_match = re.search(quantity_reg_ex,line[quantity_start_index:])
  if quantity_match:
    quantity = quantity_match.group(0)
  return (correct_package(package),correct_number(quantity),correct_number(price))

def get_market(line):
    markets = [('Tuš',r'Tuš|Tus|tusslovenija'),
                   ('DM',r'dm|drogerie|DM'),
                   ('Spar',r'[ŠSsš]par'),
                   ('Eurospin',r'[Ee]urospin'),
                   ('Kompas',r'Kompas|Shop'),
                   ('Hofer',r'HOFER'),
                   ('Lidl',r'Lidl'),
                   ('Mercator',r'[Mm][se]r[ck]ator')
                   ]
    for (market,reg_ex) in markets:
      # print(market + ' ' +reg_ex)
      match = re.search(reg_ex, line)
      if match:
        # print(reg_ex +">>>"+match.group(0))
        return market
    return None

def get_price(line):
  price_errors = [
                  ('x.xx__', r'[0-9oDOzZil][\.,][0-9oODzZil]{2}(\s|\.)?[xbABC0D8€£4\.]?(\s[x4z]?[\.:]?)?$'),
                  ('xxx__', r'[0-9oODzZil]{4,5}(\s|\.)?[xbABC0D8€£4]?:?$'),
                  # ('k9', r'(\d{3}|\d[\.,]\d{2})\s?[xbABC08€]?\.?$'),
                   ]
  # print(line)
  for (error,reg_ex) in price_errors:
    # print(error + ' ' +reg_ex)
    match = re.search(reg_ex, line)
    if match:
      # print(reg_ex+'>>>'+match.group(0))
      if error == 'x.xx__':
        return line[match.start():match.start()+4]
      elif error == 'xxx__':
        return line[match.start()]+'.'+line[match.start()+1:match.start()+3]
  return ''

def is_not_blank(line):
  return len(line.strip()) > 0

def correct_number(price):
  if price:
    price = re.sub(r'[Zz]','2', price)
    price = re.sub(r'[DOo]','0', price)
    price = re.sub(r'S$','9', price)
    price = re.sub(r',','.', price)
  return price
def correct_product(item):
  if item:
    item = re.sub(r'^[ABCDE]\s((4|H|ii|ti|dt|£)\s)?','', item)
  return item
def split_item(line):
  item_reg_ex = r'^([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z£\.\',]{1,}[\.\',:]?\s?)+([0-9]+\s+([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)+|([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)*)'
  second_line_reg_ex=r'^([0-9zZDo]+[\.,][0-9zZDo]{2,}|[1-9zZ]+\s?[xX]|\-\d+)'
  second_line_quantity_reg_ex=r'^([0-9zZDo]+[\.,][0-9zZDo]+|[0-9zZD]+)(\s?[a-z]{2,3})?'
  product=''
  package=''
  quantity=''
  price=''
  first_line=True;
  # main_line = True
  # match item line
  # print('inside '+line)
  second_line_match = re.search(second_line_reg_ex, line)
  if second_line_match:
    print('SECOND LINE '+line)
    first_line=False;
    quantity_match=re.search(second_line_quantity_reg_ex,line)
    if quantity_match:
      quantity = correct_number(quantity_match.group(0))
      price=correct_number(get_price( line[quantity_match.end():]))
  else:
    match = re.search(item_reg_ex, line, re.UNICODE)
  # print(match)
    if (match):
      print('ITEM: '+match.group(0))
      product = match.group(0)
      rest_of_item = line[match.end():]
      (package, quantity,price)=get_package_quantity_price(rest_of_item)
  return(correct_product(product),package,quantity,price,first_line)

def ocr_receipt_file_to_json(file_path):
# Define a list of known units of measurement
  with open(file_path, 'r', encoding='utf-8') as file:
      file_content = file.read()
  return  ocr_receipt_to_json(file_content)

# def process_market()
def ocr_receipt_to_json(receipt_txt):
# Define a list of known units of measurement
  item_begin_reg_ex = '([Cc]ena|Artikel|[Kk]oličina|Kol\.|Vrednost|[Zz]nesek|[Ii]zdelek|ELJR|^\s*EUR\s*$)'
  item_skip_reg_ex = '(^[^0-9]+\s*:\s+|[Pp]opust|[Kk]upon|[Pp]rihranek|[Aa]kcija)'
  footer_begin_regex='(Attendant|AUtenidant|ECR|V[IT]SA|[Ss]?kupa[jJ]|[Zz]?a plačilo|[Zz]nesek|[Kk]artica|[Pp]otrdilo|[Kk]upca|Super cena|Način|Pridobili)'
  position = {
      "market": True,
      "header": False,
      "items": False,
      "footer": False
  }
  market_found=False
  json_receipt = {"market":"",
                  "items":[]}
  # print(pattern)
  json_items = []
  item_index=-1;
  lines = receipt_txt.splitlines()
  line_index=0
  # check header for market and start of items
  for line in lines:
    if is_not_blank(line):
        market=get_market(line)
        if market:
          json_receipt["market"] = market;
          break
  for line in lines:
    line_index += 1
    match = re.search(item_begin_reg_ex, line)
    if match:
      print('ITEMS:' +line)
      position["items"] = True
      position["header"] = False
      break
  if line_index == len(lines):
    line_index = 0
    position["items"] = True
# iterate through items (even if there is not math for market or items header)
  for line in lines[line_index:]:
    if is_not_blank(line):
      if position["items"]:
        # print(line)
        if re.search(item_skip_reg_ex+'|'+item_begin_reg_ex,line):
          # print('IGNORED:'+line)
          continue
        # print(line)
        matches_items = re.findall(footer_begin_regex, line)
        if len(matches_items) > 0:
          # print(matches_items)
          position["items"] = False
          position["footer"] = True
          break
        (product_name,package_info,quantity,price,main_line) = split_item(line)
        if main_line:
          json_node = {
            "name": product_name,
            "package":package_info,
            "quantity":quantity,
            "price": price
            }
          if (product_name==''):
            continue
          json_items.append(json_node)
          item_index += 1
        else:
          if (item_index >= 0):
            json_items[item_index]["quantity"]=quantity
            if json_items[item_index]["price"] == '':
              json_items[item_index]["price"] = price
  # real_items=[]
  # for item in json_items:
  #   if item["quantity"] != '' and item["price"] != '' and item["package"] != '':
  #     real_items.append(item)
  # json_receipt["items"]=real_items
  json_receipt["items"]=json_items
  return json_receipt
# dir_path = '/content/drive/My Drive/FINKI/Projects/FOODITY/Receipts/processed/interspar-3/'
# file_name='interspar-3.txt'
# receipt_txt=ocr_receipt_file_to_json(dir_path+file_name)
# print(receipt_txt)
# Save the text data as a JSON file
# with open(dir_path+'hofer-1.json', 'w', encoding='utf-8') as json_file:
#     json.dump(receipt_txt, json_file,ensure_ascii=False, indent=4)
