import pandas as pd

# read food_labels.tsv
food_labels = pd.read_csv('data/food-labels_fillna.tsv', sep='\t')


# Define the Nutri-Score calculation function
def calculate_nutriscore(energy_kj, sugars_g, saturated_fat_g, sodium_mg, fruits_vegetables_percent, fiber_g,
                         protein_g):
    def calculate_negative_points(energy_kj, sugars_g, saturated_fat_g, sodium_mg):
        energy_points = min(10, energy_kj / 335)  # 335 kJ per point, max 10 points
        sugar_points = min(10, sugars_g / 4.5)  # 4.5 g per point, max 10 points
        saturated_fat_points = min(10, saturated_fat_g / 1)  # 1 g per point, max 10 points
        sodium_points = min(10, sodium_mg / 90)  # 90 mg per point, max 10 points

        return energy_points + sugar_points + saturated_fat_points + sodium_points

    def calculate_positive_points(fruits_vegetables_percent, fiber_g, protein_g):
        fruit_veg_points = 0
        if fruits_vegetables_percent > 80:
            fruit_veg_points = 5
        elif fruits_vegetables_percent > 60:
            fruit_veg_points = 2
        elif fruits_vegetables_percent > 40:
            fruit_veg_points = 1

        fiber_points = min(5, fiber_g / 1.2)  # 1.2 g per point, max 5 points
        protein_points = min(5, protein_g / 1.6)  # 1.6 g per point, max 5 points

        return fruit_veg_points + fiber_points + protein_points

    negative_points = calculate_negative_points(energy_kj, sugars_g, saturated_fat_g, sodium_mg)
    positive_points = calculate_positive_points(fruits_vegetables_percent, fiber_g, protein_g)

    total_score = negative_points - positive_points

    if total_score <= -1:
        nutriscore = 'A'
    elif total_score <= 2:
        nutriscore = 'B'
    elif total_score <= 10:
        nutriscore = 'C'
    elif total_score <= 18:
        nutriscore = 'D'
    else:
        nutriscore = 'E'

    return nutriscore, total_score


# Read the Excel file
file_path = 'data/IND_food_W.xlsx'
df = pd.read_excel(file_path)
df.fillna(0, inplace=True)

strong_indicators = ["SUGAR", "STARCH", "FOLAC", "VITK1", "FIBT", "VITA", "VITC", "K", "CA", "MG"]


# Normalize the values
def normalize_series(series):
    return (series - series.min()) / (series.max() - series.min())


# Weights for each indicator (can be adjusted based on domain knowledge)
weights = {
    "SUGAR": 0.2,
    "STARCH": 0.1,
    "FOLAC": 0.15,
    "VITK1": 0.1,
    "FIBT": 0.2,
    "VITA": 0.1,
    "VITC": 0.1,
    "K": 0.05,
    "CA": 0.05,
    "MG": 0.05
}

for indicator in strong_indicators:
    df[f'{indicator}_quartile'] = pd.qcut(df[indicator], q=4, labels=False, duplicates='drop')

# Calculate the fruit_veg_score by summing the weighted normalized values
df['fruit_veg_score'] = sum(df[f'{indicator}_quartile'] * weights[indicator] for indicator in strong_indicators)

# Assuming the relevant columns are present in the dataframe
df['NutriScore'], df['TotalScore'] = zip(*df.apply(
    lambda row: calculate_nutriscore(
        row['ENERC_kJ'],  # Energy in kJ
        row['SUGAR'],  # Sugars in grams
        row['FASAT'],  # Saturated Fat in grams (use FATAN if applicable)
        row['NA'],  # Sodium in mg (use NACLAD if applicable)
        row['fruit_veg_score'] if 'fruit_veg_score' in row else 0,
        # Fruits and Vegetables percentage, default to 0 if not present
        row['FIBT'],  # Fiber in grams
        row['PROT']  # Protein in grams
    ), axis=1))

# Display the resulting dataframe with Nutri-Score
print(df[['FOOD_ID', 'NAME_ENG', 'NutriScore', 'TotalScore']])

# merge the food labels with the d
#
extended_food_labels = pd.merge(food_labels, df, left_on='id', right_on='FOOD_ID', how='left')

extended_food_labels['nutriscore'] = extended_food_labels['nutriscore'].fillna(extended_food_labels['NutriScore'])

extended_food_labels.get(['id', 'bio', 'v_label', 'nutriscore']).to_csv('data/food-labels_fillna.tsv', sep='\t',
                                                                        index=False)

# fat_score as int: 0 (<3 g); 1 (3-20 g); 2 (>20 g)
df['fat_score'] = pd.cut(df['FAT'], bins=[0, 3, 20, float('inf')], labels=[0, 1, 2])
df['fat_score'] = df['fat_score'].cat.add_categories('3').fillna('3').astype(int)

# FASAT_score: GREEN (<1 g); ORANGE (1-5 g); RED (>5 g)
df['fasat_score'] = pd.cut(df['FASAT'], bins=[0, 1, 5, float('inf')], labels=[0, 1, 2])
df['fasat_score'] = df['fasat_score'].cat.add_categories('3').fillna('3').astype(int)

# SUGAR_score: GREEN (<5 g); ORANGE (5-15 g); RED (>15 g)
df['sugar_score'] = pd.cut(df['SUGAR'], bins=[0, 5, 15, float('inf')], labels=[0, 1, 2])
df['sugar_score'] = df['sugar_score'].cat.add_categories('3').fillna('3').astype(int)
# NACL_score: GREEN (<0.3 g); ORANGE (0.3-1.5 g); RED (>1.5 g)
df['nacl_score'] = pd.cut(df['NACL'], bins=[0, 0.3, 1.5, float('inf')], labels=[0, 1, 2])
df['nacl_score'] = df['nacl_score'].cat.add_categories('3').fillna('3').astype(int)

df['tl_sum'] = (df['fat_score'] + df['fasat_score'] + df['sugar_score'] + df['nacl_score'])
df['tl_score'] = df['tl_sum'] / 4

extended_food_labels = pd.merge(food_labels, df, left_on='id', right_on='FOOD_ID', how='left')

(extended_food_labels.get(['id', 'bio', 'v_label', 'nutriscore', 'tl_score'])
 .to_csv('data/food-labels_tl.tsv', sep='\t', index=False))
