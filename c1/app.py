from flask import Flask, request, jsonify

from c1_image_recognition import predict_food2k

app = Flask(__name__)


# Flask route to handle image upload and prediction
@app.route('/predict-food2k', methods=['POST'])
def handle_prediction():
    if 'image' not in request.files:
        return jsonify({"error": "No image file provided"}), 400

    image_file = request.files['image']

    # Predict the label of the uploaded image
    try:
        predicted_label = predict_food2k(image_file)
        return jsonify({"predicted_label": predicted_label})
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
