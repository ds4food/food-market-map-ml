import os

import numpy as np
import pandas as pd
import torch
from PIL import Image
from torchvision import transforms
from transformers import AutoModelForImageClassification

# Image transformations
_transforms = transforms.Compose([
    transforms.Resize((224, 224)),  # Adjust the size according to your model
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])

# Function to prepare the model
def prepare_model(model_dir='./model-data', top_path='best_model_weights', seed=1, device='cuda'):
    np.random.seed(seed)
    torch.manual_seed(seed)
    device = torch.device(device)

    # Assuming 'food2k_labels.xlsx' is located in the volume mapped folder
    labels_path = os.path.join(model_dir, 'food2k_labels.xlsx')
    indx_labels = pd.read_excel(open(labels_path, 'rb'), sheet_name='PDFTables.com')[['Index', 'Category']]
    indx_labels = indx_labels.set_index('Index')['Category'].to_dict()

    # Load the model weights from the specified path
    model_weights_path = os.path.join(model_dir, top_path)
    model = AutoModelForImageClassification.from_pretrained(
        "microsoft/swin-base-patch4-window7-224",
        num_labels=2000,
        id2label=indx_labels,
        label2id={c: str(i) for i, c in indx_labels.items()},
        ignore_mismatched_sizes=True,
    )

    model = model.from_pretrained(model_weights_path).to(device)
    return model, indx_labels

# Load the model once, globally
device = 'cuda' if torch.cuda.is_available() else 'cpu'
model, indx_labels = prepare_model(device=device)

# Function to predict the food category
def predict_food2k(image):
    img = Image.open(image)
    im = _transforms(img)
    im = im.unsqueeze(0)  # Add batch dimension
    model_input = {"pixel_values": im.to(device)}
    with torch.no_grad():
        predictions = model(**model_input)
    predicted_label = indx_labels[float(torch.argmax(predictions.logits, dim=1))]
    return predicted_label

