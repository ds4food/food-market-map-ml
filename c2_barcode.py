
from pyzbar import pyzbar
from PIL import Image
import os


# Function to read barcode from image
def read_barcode(image_path):
    image = Image.open(image_path)
    barcodes = pyzbar.decode(image)
    
    if barcodes:
        barcode_data = barcodes[0].data.decode("utf-8")
        return barcode_data
    else:
        return None
