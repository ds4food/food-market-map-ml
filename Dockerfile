# Use the official Python image from the Docker Hub
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Install libzbar0 and other system dependencies
RUN apt-get update && \
    apt-get install -y \
    tesseract-ocr \
    tesseract-ocr-slv \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libglib2.0-0 \
    libzbar0 && \
    rm -rf /var/lib/apt/lists/*


# Copy the requirements.txt file into the container
COPY requirements.txt /app/requirements.txt

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app




# Make port 5000 available to the world outside this container
EXPOSE 5000

# Define environment variable to inform Flask of the app module
ENV FLASK_APP=app.py

# Run the application
CMD ["flask", "run", "--host=0.0.0.0"]
