
import json
import os
import glob
import numpy as np
import cv2
import matplotlib.pyplot as plt
import pytesseract
import re

def read_dataset(directory_path, grayscale):
    dataset = []
    for filename in glob.glob(os.path.join(directory_path, '*')):
        dataset.append([cv2.imread(filename), filename])
        print(filename)
    return dataset

def read_dataset_gray(directory_path):
    dataset = []
    # Traverse the directory and store png images and theri file paths in a list
    for filename in glob.glob(os.path.join(directory_path, '*')):
        dataset.append([cv2.imread(filename,  cv2.IMREAD_GRAYSCALE), filename])
        print(filename)
    return dataset

# Binarize images
# useful resource: https://medium.com/analytics-vidhya/enhance-a-document-scan-using-python-and-opencv-9934a0c2da3d
def resize_receipt(src_dir,dest_dir):
    for filename in glob.glob(os.path.join(src_dir, '*')):
        img = cv2.imread(filename)
        resized = opencv_resize(img, 0.3)
        cv2.imwrite(dest_dir + os.path.basename(filename), resized)
        count += 1
    print(count)
def binarize_receipts(src_dir, dest_dir):
    for filename in os.listdir(src_dir):
        if filename.endswith('png'):
            print(filename)
            purename= os.path.splitext(os.path.basename(filename))[0]
            extension = os.path.splitext(os.path.basename(filename))[1]
            img = cv2.imread(src_dir + filename)
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            #         Thresholding image with based on local values
            binary = cv2.adaptiveThreshold(gray.astype(np.uint8),255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,21,13)
            cv2.imwrite(dest_dir + purename+extension, binary)

def tesseract_receipts(src_dir, dest_dir):
    for filename in os.listdir(src_dir):
        if filename.endswith('png'):
            print(filename)
            purename= os.path.splitext(os.path.basename(filename))[0]
            img = cv2.imread(src_dir + filename)
            extracted_text = pytesseract.image_to_string(img, lang='slv', config='--psm 4')
            # print(extracted_text)
            with open(dest_dir + purename+'.txt', 'w') as file:
                file.write(extracted_text)

def get_package_info(line):
    package_errors = [('k9',r'^\d+k9$'),
                      ('KG',r'^(\d+|\d+\.|\d+)KG$'),
                      ('G',r'^(\d+|\d+\.|\d+)G$'),
                      ('g',r'^\d{2,4}[693a]$'),
                      ('xg',r'^(\d+[xX]\d+[369a])$'),
                      ('ml',r'^\d+(m1|HL|m\])$'),
                      ('2x',r'[zZ] x'),
                      ('%m.m',r'^\d+2m.m$'),
                      ('L',r'^(\d\.\d1|\d1)$'),
                      ]
    # package_reg_ex = r'^(\d+|\d+[\.,]\d+)[0124578]$'
    package_reg_ex = r'^(\d+|\d+[\.,]\d+)'
    price_reg_ex = r'(\d[\.,]\d{2,3}\s?[xbABC08€£]?(\sx\.)?|\d{4,5}\s?[xbABC08€£\.]?(\s[x4]?[\.:]?))$'
    # print('QUANTITUY  '+quantity)
    # print(line)
    match = re.search(price_reg_ex, line)
    if match:
        line =  line[0:match.start()]
        # print('MATCH ', match.span())
    package_info_list = line.split()
    # print(package_info_list)
    quantity = ''
    if len(package_info_list) == 0:
        return ('','')
    else:
        # if firts item is a number, ignore it, otherwise it is a package info
        print('L I N E (0):' + package_info_list[0] )
        # if len(package_info_list) > 1:
        package_match = re.search(package_reg_ex, package_info_list[0])
        if not package_match:
            #  print(quantity_match.group(0))
            line = package_info_list[0]
            if len(package_info_list) > 1 and ( package_info_list[1]=='6' or
                                                package_info_list[1]=='9' or
                                                package_info_list[1]=='3' or
                                                package_info_list[1]=='a'):
                line = line + package_info_list[1]
        else:
            qunatity = package_info_list[0]
            package_info = ''
            return ('', qunatity)

        for (error,reg_ex) in package_errors:
            # print(error + ' ' +reg_ex)
            match = re.search(reg_ex, line)
            if match:
                # print(reg_ex+'>>>'+match.group(0))
                if error == 'KG':
                    line = line[:match.end()-2]+ 'kg'
                elif error == 'k9':
                    line = line[:match.end()-1]+ 'kg'
                elif error == 'G':
                    line = line[:match.end()-1]+ 'g'
                elif error == 'g':
                    number = int(line[match.start():match.end()-1])
                    # print('NUMBBER = '+number)
                    if  number < 100 or number % 5 == 0:
                        line = line[:match.end()-1]+ 'g'
                elif error == 'xg':
                    line = line[:match.end()-1]+ 'g'
                    line = line.replace("X","x")
                elif error == 'ml':
                    line = line[:match.end()-2]+  error
                elif error == 'L':
                    line = line[:match.end()-1]+  error
                elif error == '%m.m':
                    line = line[:match.end()-4]+ error
                elif error == '2x':
                    line[match.start()] = '2'
                if len(package_info_list) > 1:
                    quantity = package_info_list[1]
    return (line, quantity)

def get_market(line):
    markets = [('Tuš',r'Tuš|Tus|tusslovenija'),
               ('DM',r'dm|drogerie|DM'),
               ('Spar',r'[Ss]par'),
               ('Eurospin',r'[Ee]urospin'),
               ('Kompas',r'Kompas|Shop'),
               ('Hofer',r'HOFER'),
               ('Lidl',r'Lidl'),
               ('Mercator',r'[Mm][se]r[ck]ator')
               ]
    for (market,reg_ex) in markets:
        # print(market + ' ' +reg_ex)
        match = re.search(reg_ex, line)
        if match:
            # print(reg_ex +">>>"+match.group(0))
            return market
    return None

def get_price(line):
    price_errors = [
        ('x.xx__', r'[0-9oDOzZil][\.,][0-9oODzZil]{2}(\s|\.)?[xbABC0D8€£4\.]?(\s[x4z]?[\.:]?)?$'),
        ('xxx__', r'[0-9oODzZil]{4,5}(\s|\.)?[xbABC0D8€£4]?:?$'),
        # ('k9', r'(\d{3}|\d[\.,]\d{2})\s?[xbABC08€]?\.?$'),
    ]
    # print(line)
    for (error,reg_ex) in price_errors:
        # print(error + ' ' +reg_ex)
        match = re.search(reg_ex, line)
        if match:
            # print(reg_ex+'>>>'+match.group(0))
            if error == 'x.xx__':
                return line[match.start():match.start()+4]
            elif error == 'xxx__':
                return line[match.start()]+'.'+line[match.start()+1:match.start()+3]
    return ''

def is_not_blank(line):
    return len(line.strip()) > 0
def correct_price(price):
    if price:
        price = re.sub(r'[Zz]','2', price)
        price = re.sub(r'[DOo]','0', price)
        price = re.sub(r'S$','9', price)
        price = re.sub(r',','.', price)
    return price
def correct_product(item):
    if item:
        item = re.sub(r'^[ABCDE]\s((4|H|ii|ti|dt|£)\s)?','', item)
    return item
def split_item(line):
    # item_reg_ex = r'([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]+[\.\',:]?\s?)+'

    # item_reg_ex = r'^([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z£\.\',]{1,}[\.\',:]?\s?)+([0-9]+\s+)?([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)*'
    item_reg_ex = r'^([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z£\.\',]{1,}[\.\',:]?\s?)+([0-9]+\s+([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)+|([\u010C\u010D\u0160\u0161\u017D\u017Ea-zA-Z\.\',]{3,}[\.\',:]?\s?)*)'
    # qunatity_reg_ex = r'^(\d+|\d+[\.,]\d+)[^369]$'
    # price_reg_ex = r'(\d{3}|\d[\.,]\d{2})\s?[xbABC08€]?\.?$'\
    second_line_reg_ex=r'^([0-9zZDo]+[\.,][0-9zZDo]{2,}|[0-9zZDo]+\s?\[^0-9]|\-\d+)'
    second_line_quantity_reg_ex=r'^([0-9zZDo]+[\.,][0-9zZDo]+|[0-9zZD]+)(\s?[a-z]{2,3})?'
    product_name=''
    package_info=''
    quantity=''
    price=''
    first_line=True;
    # main_line = True
    # match item line
    # print('inside '+line)
    match = re.search(item_reg_ex, line, re.UNICODE)
    # print(match)
    if (match):
        # print('ITEM: '+match.group(0))
        product_name = match.group(0)
        rest_of_item = line[match.end():]
        (package_info, quantity)=get_package_info(rest_of_item)
        # price=get_price(rest_of_item)
        price=get_price(line)

    else:
        second_line_match = re.search(second_line_reg_ex, line)
        if second_line_match:
            # print('SECOND LINE '+line)
            first_line=False;
            quantity_match=re.search(second_line_quantity_reg_ex,line)
            if quantity_match:
                quantity = quantity_match.group(0)
                price=get_price( line[quantity_match.end():])
            # print('PRICE:'+price)
            # price_match = re.search(price_reg_ex,line)
            # if price_match:
            #   price = price_match.group(0)
    # print(product_name+'>>> '+package_info+'>>> '+quantity+'>>> '+price)
    return(correct_product(product_name),package_info,quantity,correct_price(get_price(price)),first_line)

def ocr_receipt_file_to_json(file_path):
    # Define a list of known units of measurement
    with open(file_path, 'r', encoding='utf-8') as file:
        file_content = file.read()
    return  ocr_receipt_to_json(file_content)

# def process_market()
def ocr_receipt_to_json(receipt_txt):
    # Define a list of known units of measurement
    item_begin_reg_ex = '([Cc]ena|Artikel|[Kk]oličina|Kol\.|Vrednost|[Zz]nesek|[Ii]zdelek|^\s*EUR\s*$)'
    item_skip_reg_ex = '(^[^0-9]+\s*:\s+|[Pp]opust|[Kk]upon|[Pp]rihranek|[Aa]kcija)'
    footer_begin_regex='(Attendant|AUtenidant|ECR|V[IT]SA|[Ss]?kupa[jJ]|[Zz]?a plačilo|[Zz]nesek|[Kk]artica|[Pp]otrdilo|[Kk]upca|Super cena|Način|Pridobili)'
    position = {
        "market": True,
        "header": False,
        "items": False,
        "footer": False
    }
    market_found=False
    json_receipt = {"market":"",
                    "items":[]}
    # print(pattern)
    json_items = []
    item_index=-1;
    lines = receipt_txt.splitlines()
    line_index=0
    # check header for market and start of items
    for line in lines:
        if is_not_blank(line):
            market=get_market(line)
            if market:
                json_receipt["market"] = market;
                break
    for line in lines:
        line_index += 1
        match = re.search(item_begin_reg_ex, line)
        if match:
            print('ITEMS:' +line)
            position["items"] = True
            position["header"] = False
            break
    if line_index == len(lines):
        line_index = 0
        position["items"] = True
    # iterate through items (even if there is not math for market or items header)
    for line in lines[line_index:]:
        if is_not_blank(line):
            if position["items"]:
                # print(line)
                if re.search(item_skip_reg_ex+'|'+item_begin_reg_ex,line):
                    # print('IGNORED:'+line)
                    continue
                # print(line)
                (product_name,package_info,quantity,price,main_line) = split_item(line)
                matches_items = re.findall(footer_begin_regex, product_name)
                if len(matches_items) >0:
                    # print(matches_items)
                    position["items"] = False
                    position["footer"] = True
                    break
                if main_line:
                    json_node = {
                        "name": product_name,
                        "package":package_info,
                        "quantity":quantity,
                        "price": price
                    }
                    if (product_name==''):
                        continue
                    json_items.append(json_node)
                    item_index += 1
                else:
                    json_items[item_index]["quantity"]=quantity
                    if json_items[item_index]["price"] == '':
                        json_items[item_index]["price"] = price
    json_receipt["items"]=json_items
    return json_receipt
import shutil
import os
def tesseract_receipts(src_dir, dest_dir):
    for filename in os.listdir(src_dir):
        if filename.endswith('png'):
            print(filename)
            purename= os.path.splitext(os.path.basename(filename))[0]
            os.makedirs(dest_dir+purename, exist_ok=True)

            img = cv2.imread(src_dir + filename)
            extracted_text = pytesseract.image_to_string(img, lang='slv', config='--psm 4')
            with open(dest_dir + purename+'/'+purename+'.txt', 'w') as file:
                file.write(extracted_text)

            postprocessed_json_txt=ocr_receipt_to_json(extracted_text)

            with open(dest_dir+purename+'/'+purename+'.json', 'w', encoding='utf-8') as json_file:
                json.dump(postprocessed_json_txt, json_file,ensure_ascii=False, indent=4)
            with open(dest_dir+purename+'/'+purename+'_validate.json', 'w', encoding='utf-8') as json_file:
                json.dump(postprocessed_json_txt, json_file,ensure_ascii=False, indent=4)

            shutil.copy(src_dir+filename,dest_dir+purename+'/'+filename)
            # print(extracted_text)

src_dir='/content/drive/My Drive/FINKI/Projects/FOODITY/Receipts/binarized/'
dest_dir='/content/drive/My Drive/FINKI/Projects/FOODITY/Receipts/processed/'
tesseract_receipts(src_dir,dest_dir)
